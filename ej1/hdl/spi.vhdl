

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity spi_master_int is
    generic (
        -- Width of M_SPI data bus
        C_M_SPI_DATA_WIDTH  : integer   := 32;
        -- Width of M_SPI address bus
        C_M_SPI_ADDR_WIDTH  : integer   := 4;
		-- AXI_CLK_FREQ / SPI_CLK_FREQ
		C_M_CLK_FREQ_RATIO : integer := 100
    );
    port (
        -- Users to add ports here
        WDATA_I         : in std_logic_vector(C_M_SPI_DATA_WIDTH-1 downto 0);
        RDATA_O         : out std_logic_vector(C_M_SPI_DATA_WIDTH-1 downto 0);
        WENA_I          : in std_logic;
        RENA_I          : in std_logic;
        RADDR_I         : in std_logic_vector(C_M_SPI_ADDR_WIDTH-1 downto 0);
        WADDR_I         : in std_logic_vector(C_M_SPI_ADDR_WIDTH-1 downto 0);
        -- AXI4-Lite Slave Port
        AXI_ACLK      	: in std_logic;
        AXI_ARESETN   	: in std_logic;
		-- SPI Signals
        MOSI			: out std_logic;
        MISO			: in std_logic;
        SCLK			: out std_logic;
        CSn				: out std_logic;
        INT				: out std_logic
    );
end spi_master_int;

architecture arch_imp of spi_master_int is
    
    constant SPI_CLK_HALF_PERIOD: positive := C_M_CLK_FREQ_RATIO/2;
    constant SPI_FRAME_LENGTH: positive := 8;
	signal WDATA  : std_logic_vector(C_M_SPI_DATA_WIDTH-1 downto 0);
    signal RDATA  : std_logic_vector(C_M_SPI_DATA_WIDTH-1 downto 0);
	signal clk_cnt: std_logic_vector(integer(ceil(log2(real(SPI_CLK_HALF_PERIOD))))-1 downto 0);
	signal bit_cnt: std_logic_vector(integer(ceil(log2(real(SPI_FRAME_LENGTH))))-1 downto 0);
	constant TX_ADDR: natural := 0;
	constant RX_ADDR: natural := 4;
	signal CSn_int: std_logic;
	signal SCLK_int: std_logic;
	
begin
	
	write_proc: process (AXI_ACLK)
    begin
        if rising_edge(AXI_ACLK) then 
            if AXI_ARESETN = '0' then
                WDATA <= (others => '0');
				RDATA <= (others => '0');
				SCLK_int <= '0';
				CSn_int <= '1';
				clk_cnt <= (others => '0');
				bit_cnt <= (others => '0');
				INT <= '0';
            else
				INT <= '0';
				if(CSn_int='0')then
					if(unsigned(clk_cnt) = SPI_CLK_HALF_PERIOD-1)then
						clk_cnt <= (others => '0');
						SCLK_int <= not(SCLK_int);
						if(SCLK_int = '1')then
							if(unsigned(bit_cnt) = SPI_FRAME_LENGTH-1)then
								bit_cnt <= (others => '0');
								CSn_int <= '1';
								INT <= '1';
							else
								bit_cnt <= std_logic_vector(unsigned(bit_cnt)+1);
							end if;
						else
							RDATA(SPI_FRAME_LENGTH-1-to_integer(unsigned(bit_cnt))) <= MISO;
						end if;
					else
						clk_cnt <= std_logic_vector(unsigned(clk_cnt)+1);
					end if;
				else
					if (WENA_I = '1' and unsigned(WADDR_I) = TX_ADDR) then
						WDATA <= WDATA_I;
						CSn_int <= '0';
					end if;
				end if;
            end if;
        end if;
    end process;
	
	CSn <= CSn_int;
	SCLK <= SCLK_int;
	MOSI <= WDATA(SPI_FRAME_LENGTH-1-to_integer(unsigned(bit_cnt)));
	
	read_proc: process (AXI_ACLK)
    begin
        if rising_edge(AXI_ACLK) then 
            if AXI_ARESETN = '0' then
				RDATA_O <= (others => '0');
            else
				if (RENA_I = '1') then
					case to_integer(unsigned(RADDR_I)) is
						when TX_ADDR =>
							RDATA_O <= WDATA;
						when RX_ADDR =>   
							RDATA_O <= RDATA;
						when others => 
							RDATA_O <= (others => '0');
					end case;
				end if;
            end if;
        end if;
    end process;
	
end arch_imp;