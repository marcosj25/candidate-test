import cocotb
import random

from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, ReadOnly
from cocotb.result import TestFailure, TestError, ReturnValue, SimFailure
from cocotb.binary import BinaryValue

from cocotb.utils import get_sim_time

CLK_PERIOD_A = 2
CLK_PERIOD_B = 4

@cocotb.test()
def simple_test(dut):
    """
        Full test: perform a write and read operation in all the address range.
    """

    dut._log.info('Init Clocks')
    cocotb.fork(Clock(dut.CLKA, CLK_PERIOD_A, units='ns').start())
    cocotb.fork(Clock(dut.CLKB, CLK_PERIOD_B, units='ns').start())

    dut._log.info('Setup the initial state of signals')
    dut.ENA <= 1 # Always enable
    dut.ENB <= 1 # Always enable
    dut.WEA <= 0

    dut.ADDRA <= 0
    dut.ADDRB <= 0

    dut.DIA <= 0
    dut.DOB <= 0
	
    for addr in range(dut.MEM_SIZE.value.integer):
        yield RisingEdge(dut.CLKA)
        yield RisingEdge(dut.CLKA)
        
        val = round(random.uniform(0,2**(dut.DATA_SIZE.value.integer)-1))
        
        dut._log.info('Value {} in address {}'.format(hex(val),hex(addr)))
        dut.WEA <= 1
        dut.ADDRA <= addr
        dut.DIA <= val
        yield RisingEdge(dut.CLKA)
        dut.WEA <= 0
        dut.ADDRB <= addr
        yield ReadOnly()

        # It's a synch read memory, so you need 2 cycles to read the real data

        yield RisingEdge(dut.CLKB)
        yield RisingEdge(dut.CLKB)

        if (dut.DOB.value.integer != dut.DIA.value.integer):
            raise TestFailure("Data read in address {} is not correct".format(dut.ADDRB))
